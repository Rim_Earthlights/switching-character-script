// ==UserScript==
// @name         Paimon.moe Switching Character Script
// @namespace    http://rim-linq.net
// @version      0.3
// @description  Switching Character Script for Paimon.moe
// @author       Rim Earthlights
// @match        https://paimon.moe/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=paimon.moe
// @downloadURL  https://gitlab.com/Rim_Earthlights/switching-character-script/-/raw/main/switching-character-script.user.js
// @updateURL    https://gitlab.com/Rim_Earthlights/switching-character-script/-/raw/main/switching-character-script.user.js
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
    addEventListener("keydown", event => {
        if (event.isComposing) {
            return;
        }

        switch(event.code) {
            case 'Digit0':
                showDigitCharacters(0);
                break;
            case 'Digit1':
                showDigitCharacters(1);
                break;
            case 'Digit2':
                showDigitCharacters(2);
                break;
            case 'Digit3':
                showDigitCharacters(3);
                break;
            case 'Digit4':
                showDigitCharacters(4);
                break;
            case 'Digit5':
                showDigitCharacters(5);
                break;
            case 'Digit6':
                showDigitCharacters(6);
                break;
            case 'KeyC':
                switchCharacters();
                break;
        }
    });
})();

const allShowCharacters = () => {
    const characters = document.querySelector('.max-w-screen-xl').children;

    for(const character of characters) { 
        if (character.style.display === 'none') {
            character.style.display = '';
        }    
    }
};


const switchCharacters = () => {
    const characters = document.querySelector('.max-w-screen-xl').children;
    // console.log(characters);
    let flag = false;
    for(const character of characters) { 
        if (character.style.display === 'none') {
            flag = true;
            character.style.display = '';
        }    
    }

    if (flag) {
        return;
    }

    for (const character of characters) {
        const c = character.querySelector('a > div.absolute.top-0.right-0');
        const cCount = c.querySelector('span');
        if (!cCount) {
            character.style.display = 'none';
        }
    }
};

const showDigitCharacters = (num) => {
    allShowCharacters();
    const characters = document.querySelector('.max-w-screen-xl').children;
    // console.log(characters);
    for (const character of characters) {
        const c = character.querySelector('a > div.absolute.top-0.right-0');
        const cCount = c.querySelector('span');
        if (cCount) {
            const count = Number(cCount.innerText.slice(1));
            if (num === 6) {
                if (count <= 5) {
                    character.style.display = 'none';
                }
            }
            else {
                if (count !== num) {
                    character.style.display = 'none';
                }
            }

        } else {
            character.style.display = 'none';
        }
    }
};
